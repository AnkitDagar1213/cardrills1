const onlyBMWAndAudi = (data) => {
    let arr=[]
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    for (let index=0;index<data.length;index++){
        if(data[index].car_make==="BMW" || data[index].car_make==="Audi"){
            arr.push(data[index])
        }
    }
    let cars=JSON.stringify(arr,null,2)
    return cars
}
module.exports = onlyBMWAndAudi;
