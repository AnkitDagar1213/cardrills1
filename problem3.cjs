const sortedAllCarsMaker = (data) => {
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    for (let index=0;index<data.length;index++){
        arr.push(data[index].car_model)
    }
    return arr.sort();
}

module.exports = sortedAllCarsMaker;