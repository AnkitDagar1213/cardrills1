const findById = require('../problem1.cjs')
const data = require('../information.cjs')

const car = findById(data,33);
console.log(`Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`);
