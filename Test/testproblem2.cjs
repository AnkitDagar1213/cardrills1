const lastCar = require('../problem2.cjs')
const data = require('../information.cjs')

const car = lastCar(data);
console.log (`Last car is a ${car.car_make} ${car.car_model}`)