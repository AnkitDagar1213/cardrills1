const lastCar = (data) => {
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    return data[data.length-1]
}

module.exports = lastCar;