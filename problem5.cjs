const oldCarsList = (data) => {
    let arr=[];
    let count=0
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    for (let index=0;index<data.length;index++){
        if(data[index]<2000){
            count++;
        }
    }
    return count
}

module.exports = oldCarsList;