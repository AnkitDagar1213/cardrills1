const findById = (data,Id) => {
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    if(typeof Id !== 'number'){
        return arr;
    }
    for (let index=0;index<data.length;index++){
        if (data[index].id === Id) {
            return data[index];
        }
    }
}

module.exports = findById;